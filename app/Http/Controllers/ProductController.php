<?php

namespace App\Http\Controllers;

use DB;
use App\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
setlocale(LC_MONETARY,"es_MX");
class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Migrate excel data file.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        if ($request->hasFile('file'))
        {
            try
            {
                $archivo = $request->file('file');
                $product_data = Excel::load($archivo)->get();

                $insert_data = [];
                DB::beginTransaction();
                foreach ($product_data as $key => $data) {

                    $product = Product::find($data['id']);
                    if(empty($product))
                    {
                        $insert_data[] = [
                            'id'            => $data['id'],
                            'name'          => $data['name'],
                            'description'   => $data['description'],
                            'price'         => $data['price'],
                            'quantity'      => $data['quantity'],
                            'created_at'    => date('Y-m-d H:i:s')
                        ];
                    }
                    else
                    {
                        $product->name          = $data['name'];
                        $product->description   = $data['description'];
                        $product->price         = $data['price'];
                        $product->quantity      = $data['quantity'];

                        if($product->isDirty())
                        {
                            $product->save();
                        }
                    }
                }

                Product::insert($insert_data);
                DB::commit();

                return response()->json([
                    'success' => true,
                    'message' => 'Resource created',
                ], 201);
            }
            catch(\Exception $e)
            {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Error creating resource',
                ], 500);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'File not found',
            ], 400);
        }
    }

    public function export(Request $request)
    {
        $products = Product::all();

        $subtotal = 0.0;
        foreach ($products as $key => $product) {

            $product['total'] =  $product->price * $product->quantity;
            $subtotal += $product['total'];
            # code...
        }

        Excel::create('Products_export', function($excel) use($products, $subtotal) {

            $excel->sheet('products', function($sheet) use($products, $subtotal) {

                $sheet->setAllBorders('thin');
                $sheet->setColumnFormat(array(
                    'F' => '"$"#,##0.00_-',
                    'D' => '"$"#,##0.00_-'
                ));

                $iva = $subtotal * 0.16;
                $grand_total = $subtotal + $iva;
                $sheet->fromArray($products);
                $sheet->appendRow(array(
                    '','','','','SUBTOTAL', $subtotal
                ));
                $sheet->appendRow(array(
                    '','','','','IVA', $iva
                ));
                $sheet->appendRow(array(
                    '','','','','TOTAL', $grand_total
                ));

            });

        })->download('xlsx');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
