<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use App\User;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class GoogleController extends Controller
{
    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function redirectToGoogle()

    {

        return Socialite::driver('google')->redirect();
    }



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function handleGoogleCallback()
    {
        try {

            $user = Socialite::driver('google')->user();
            $finduser = User::where('google_id', $user->id)->first();

            if ($finduser)
            {
                Auth::login($finduser);
                $token = JWTAuth::fromUser($finduser);

                return response()->json([
                    'success' => true,
                    'token' => $token,
                ]);
            }
            else
            {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id' => $user->id,
                    'password' => bcrypt('123456dummy')
                ]);

                Auth::login($newUser);
                $token = JWTAuth::fromUser($finduser);

                return response()->json([
                    'success' => true,
                    'token' => $token,
                ]);
            }
        } catch (Exception $e) {

            dd($e->getMessage());
        }
    }
}
