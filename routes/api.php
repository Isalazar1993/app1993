<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login')->name('api.login');
Route::post('register', 'ApiController@register')->name('api.register');

Route::group(['middleware' => 'auth.jwt'], function () {

    Route::get('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');

    Route::post('product/import', 'ProductController@import');
    Route::get('product/export', 'ProductController@export');
});
